#!/usr/bin/env python
# coding: utf-8

# In[5]:


# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
from dash import dcc
from dash import html
import plotly.express as px
import pandas as pd
import seaborn as sns

app = dash.Dash(__name__)
server = app.server

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options

flights = pd.read_csv("flights.csv.gz")
weather = pd.read_csv("weather.csv.gz")


fig_u = flights['sched_dep_time'].value_counts().nlargest(10).plot(kind = 'bar')
df_v = weather[['month','wind_speed_LGA']].groupby('month')['wind_speed_LGA'].mean()
fig_v = df_v.plot()




app.layout = html.Div(children=[
    html.H1(children='Hello Dash'),

    html.Div(children='''
        Dash: A web application framework for your data.
    '''),

    dcc.Graph(
        id='example-graph',
        figure=fig_u
    )
    ,
    html.Div(children='''
        Dash: Another example for chart
    '''),

    dcc.Graph(
        id='example-graph2',
        figure=fig_v
    )
])

if __name__ == '__main__':
    app.run_server(debug=True, port=8080)







